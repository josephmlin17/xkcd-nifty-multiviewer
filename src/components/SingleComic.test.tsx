
import renderer from 'react-test-renderer';
import { SingleComic } from './SingleComic';
import { emptyXkcdComic } from '../model/xkcdComic';
import React from 'react';
describe('Single Comic Snapshot', () => {
  test("Empty Image", () => {
    const render = renderer.create(
      <SingleComic singleComic={emptyXkcdComic} />
    )
    expect(render).toMatchSnapshot();
  });
  test("Fake Image", () => {
    const imagedComic = {
      ...emptyXkcdComic,
      img: "www.google.com"
    }
    const render = renderer.create(
      <SingleComic singleComic={imagedComic} />
    )
    expect(render).toMatchSnapshot();
  })
})
