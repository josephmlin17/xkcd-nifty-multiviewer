import { xkcdComic } from "../model/xkcdComic";
import React from "react";
import { Typography } from "@material-ui/core";

interface SingleComicProps {
  singleComic: xkcdComic;
}

export class SingleComic extends React.Component<SingleComicProps, {}> {
  render(): JSX.Element {
    const { singleComic } = this.props;
    return (
      <div key={singleComic.num}>
        <div>
          {singleComic.title}
        </div>
        <img src={singleComic.img} title={singleComic.alt} />
        <Typography variant="caption" style={{ margin: "2em 2em 1em 2em" }} paragraph>
          {singleComic.transcript}
        </Typography>
      </div >
    );
  }
}