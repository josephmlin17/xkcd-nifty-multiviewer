
import renderer from 'react-test-renderer';
import ComicPage from './ComicPAge';
import { emptyXkcdComic } from '../model/xkcdComic';
import React from 'react';
describe('Single Comic Snapshot', () => {
  test("Empty Image", () => {
    const getComic = jest.fn();
    const render = renderer.create(
      <ComicPage comics={[emptyXkcdComic]} maxCount={3} getComic={getComic} />
    )
    expect(render).toMatchSnapshot();
  });
  test("Fake Image", () => {
    const getComic = jest.fn();
    const render = renderer.create(
      <ComicPage comics={[emptyXkcdComic]} maxCount={3} getComic={getComic} />
    )
    expect(render).toMatchSnapshot();
  })
})
