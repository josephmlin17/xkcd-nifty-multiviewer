import React from "react";
import { xkcdComic } from "../model/xkcdComic";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import { SingleComic } from "./SingleComic";
import ComicPageUtil from "./utils/ComicPageUtil";

export interface ComicDispatchProps {
  getComic: Function;
}

export interface ComicPropsFromStore {
  comics: xkcdComic[];
  maxCount: number;
}

export interface ComicPageProps extends ComicDispatchProps, ComicPropsFromStore, ComicPageOwnProps { }

export interface ComicPageOwnProps { }

export interface ComicPageState {
  pageNumber: number;
  comicsPerPage: number;
}

export class ComicPage extends React.Component<ComicPageProps, ComicPageState> {
  constructor(props: ComicPageProps) {
    super(props);
    this.state = {
      pageNumber: 0,
      comicsPerPage: 3
    }
  }

  componentDidMount() {
    ComicPageUtil.getPage(0, this.state.comicsPerPage, this.props.maxCount, this.props.getComic);
    ComicPageUtil.getPage(1, this.state.comicsPerPage, this.props.maxCount, this.props.getComic);
  }

  renderComicPage = () => {
    const pagedValue = ComicPageUtil.getComicPage(this.state.pageNumber, this.props.comics, this.state.comicsPerPage);
    return pagedValue.map((xkcdComic: xkcdComic) => (<SingleComic key={xkcdComic.num} singleComic={xkcdComic} />));
  }

  isPreviousEnabled = () => this.state.pageNumber >= 1;
  isNextEnabled = () =>
    this.state.pageNumber <= Math.ceil(this.props.maxCount / this.state.comicsPerPage);

  getPreviousColor = () => {
    return this.isPreviousEnabled() ? "primary" : "disabled";
  }

  getNextColor = () => {
    return this.isNextEnabled() ? "primary" : "disabled";
  }

  previousPage = () => {
    if (this.isPreviousEnabled()) {
      this.setState({
        pageNumber: this.state.pageNumber - 1
      });
    }
  }

  nextPage = () => {
    const nextPage = this.state.pageNumber + 1;
    if (this.isNextEnabled()) {
      if (!ComicPageUtil.isPagePopulated(nextPage + 1, this.props.comics, this.state.comicsPerPage)) {
        ComicPageUtil.getPage(nextPage + 1, this.state.comicsPerPage, this.props.maxCount, this.props.getComic);
      }
      this.setState({
        pageNumber: nextPage
      });
    }
  }

  render() {
    return (
      <>
        {this.renderComicPage()}
        <div>
          <KeyboardArrowLeft color={this.getPreviousColor()} onClick={this.previousPage} />
          <KeyboardArrowRight color={this.getNextColor()} onClick={this.nextPage} />
        </div>
      </>
    );
  }
}
export default ComicPage;  