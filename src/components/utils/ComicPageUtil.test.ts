import ComicPageUtil from "./ComicPageUtil";
import { emptyXkcdComic, xkcdComic } from "../../model/xkcdComic";

describe("Comic Page Utility tests", () => {
  it("Get Page", () => {
    const getComic = jest.fn();
    ComicPageUtil.getPage(0, 3, 100, getComic);
    expect(getComic).toBeCalledTimes(3);
  });
  it("Get Comic Page", () => {
    const comicArray: xkcdComic[] = [{ ...emptyXkcdComic, num: 0 }, { ...emptyXkcdComic, num: 1 }, { ...emptyXkcdComic, num: 2 }, { ...emptyXkcdComic, num: 3 }];
    const comicPage = ComicPageUtil.getComicPage(0, comicArray, 3);
    expect(comicPage.length).toEqual(3);
  })
  it("Is Page Populated", () => {
    const comicArray: xkcdComic[] = [{ ...emptyXkcdComic, num: 0 }, { ...emptyXkcdComic, num: 1 }, { ...emptyXkcdComic, num: 2 }, { ...emptyXkcdComic, num: 3 }];
    const isPagePopulated = ComicPageUtil.isPagePopulated(0, comicArray, 3);
    expect(isPagePopulated).toBeTruthy();
  })
});