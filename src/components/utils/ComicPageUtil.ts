import { xkcdComic } from "../../model/xkcdComic";

/**
 * Makes a fetch call to retrieve all of the existing comics
 * @param page Current Page Number
 * @param comicsPerPage Comics Per Page
 * @param maxCount Total amount of comics
 * @param getComic Dispatch function
 */
export const getPage = (page: number, comicsPerPage: number, maxCount: number, getComic: Function) => {
  for (let i = page * comicsPerPage; i < (page + 1) * comicsPerPage; i++) {
    getComic ? getComic(i + 1) : null; // Because the comics online are NOT indexed 0
  }
}

/**
 * Using the existing data, returns the comics within the page.
 * @param page current page
 * @param comics All of the existing comics
 * @param comicsPerPage Comics Per PAge
 */
export const getComicPage = (page: number, comics: xkcdComic[], comicsPerPage: number) => {
  return comics.filter((value: xkcdComic) => {
    return (value.num > page * comicsPerPage &&
      value.num <= (page + 1) * comicsPerPage)
  }).sort((a: xkcdComic, b: xkcdComic) => a.num - b.num);
}

export const isPagePopulated = (page: number, comics: xkcdComic[], comicsPerPage: number) =>
  getComicPage(page, comics, comicsPerPage).length === comicsPerPage;

export const ComicPageUtil = {
  getPage,
  getComicPage,
  isPagePopulated
};

export default ComicPageUtil;