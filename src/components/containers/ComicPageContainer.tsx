import ComicPage, { ComicDispatchProps, ComicPropsFromStore } from "../ComicPage";
import { AppStore } from "../../reducer/rootReducer";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
import { fetchComic } from "../../actions/comicActions";
import { getComicsFromStore, getMaxCountFromStore } from "../../reducer/comicReducer";

function mapStateToProps(store: AppStore): ComicPropsFromStore {
  return {
    comics: getComicsFromStore(store),
    maxCount: getMaxCountFromStore(store)
  };
}

function mapDispatchToProps(dispatch: Dispatch): ComicDispatchProps {
  return {
    // They say this is not the suggested way to do it. Will research later
    getComic: bindActionCreators(fetchComic, dispatch)
  };
}

export default connect<ComicPropsFromStore, ComicDispatchProps, {}>(mapStateToProps, mapDispatchToProps)(ComicPage);