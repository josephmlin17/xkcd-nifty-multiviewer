import { Dispatch } from "redux";

const PREFIX = "[COMIC]"

const ActionTypes = {
  FETCH_COMIC: (PREFIX + "GET_COMIC"),
  MAX_COUNT: (PREFIX + "MAX_COUNT")
}

const url = "https://www.xkcd.com/"
// I spent some time trying to figure out a bug with Create React App issue with proxying. 
// I was hoping to avoid this, but spent way too long trying to look for alternatives.
const header = "https://cors-anywhere.herokuapp.com/"

/**
 * This function receives a comic number, queries XKCD for that information, then returns the data to our store
 * @param comicNumber The comic number
 */
export const fetchComic = (comicNumber: number) => {
  return (dispatch: Dispatch) => fetch(`${header}${url}${comicNumber}/info.0.json`)
    .then(response => response.json())
    .then(json => dispatch(saveComic(json)))
    .catch(error => {
      console.log(error);
    });
}

export const getMaxCount = () => {
  return (dispatch: Dispatch) => fetch(`${header}${url}/info.0.json`)
    .then(response => response.json())
    .then(json => dispatch(saveMaxCount(json.num)))
    .catch(error => {
      console.log(error);
    })
}

const saveComic = (json: any) => {
  return {
    type: ActionTypes.FETCH_COMIC,
    payload: json
  };
}

const saveMaxCount = (id: number) => {
  return {
    type: ActionTypes.MAX_COUNT,
    payload: id
  }
}

export const ComicActionCreators = {
  fetchComic,
  getMaxCount
}

export const ComicAction = {
  ...ComicActionCreators,
  PREFIX,
  ActionTypes,
};

export default ComicAction;
