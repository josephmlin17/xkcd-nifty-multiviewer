import { Action } from "redux";

export const filterReducer = (action: Action, type: string) => {
  return (action.type as string).includes(type);
}