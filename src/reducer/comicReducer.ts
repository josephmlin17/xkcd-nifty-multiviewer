import { xkcdComic } from "../model/xkcdComic";
import { filterReducer } from "./ReducerUtils";
import { AnyAction } from "redux";
import ComicAction from "../actions/comicActions";
import { AppStore } from "./rootReducer";
import { stat } from "fs";

// This is the part of the store that contains all of the comics we've saved so far.
// We can add extra things to the state, i.e. max count
export interface ComicState {
  comics: xkcdComic[];
  maxCount: number;
};

const initialState: ComicState = { comics: [], maxCount: 0 };

export const getComicsFromStore = (store: AppStore) => store.comics ? store.comics.comics : initialState.comics;
export const getMaxCountFromStore = (store: AppStore) => store.comics ? store.comics.maxCount : initialState.maxCount;

export function comicReducer(state: ComicState = initialState, action: AnyAction): ComicState {
  if (filterReducer(action, ComicAction.PREFIX)) {
    switch (action.type) {
      case ComicAction.ActionTypes.FETCH_COMIC:
        let newState = { ...state, comics: [...state.comics, action.payload] };
        return newState;
      case ComicAction.ActionTypes.MAX_COUNT:
        let maxCountState = { ...state, maxCount: action.payload };
        return maxCountState;
      default:
        break;
    }
    return state;
  }
  return initialState;
}