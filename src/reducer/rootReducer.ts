import { combineReducers } from "redux";
import { ComicState, comicReducer } from "./comicReducer";

export const rootReducer = combineReducers({
  comics: comicReducer
});

export interface AppStore {
  comics?: ComicState
}