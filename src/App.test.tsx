import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const mockStore = configureStore();
  ReactDOM.render(
    <Provider store={mockStore}>
      <App />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
