export interface xkcdComic {
  month: string;
  num: number;
  link: string;
  year: string;
  news: string;
  safe_title: string;
  transcript: string; // Web Accessible Text
  alt: string;
  img: string;
  title: string;
  day: string;
}

export const emptyXkcdComic: xkcdComic = {
  month: '',
  num: 0,
  link: '',
  year: '',
  news: '',
  safe_title: '',
  transcript: '', // Web Accessible Text
  alt: '',
  img: '',
  title: '',
  day: ''
}