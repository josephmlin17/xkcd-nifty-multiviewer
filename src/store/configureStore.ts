import { createStore, compose, applyMiddleware } from "redux";
import { rootReducer } from "../reducer/rootReducer";
import thunk from "redux-thunk";

const composeEnhancer = (<any>window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export function configureStore() {
  return createStore(
    rootReducer,
    // This allows us to use the Redux Plugin Firefox tool
    composeEnhancer(applyMiddleware(thunk))
  );
}

const store = configureStore();

export default store;