import React, { Component } from 'react';
import './App.css';
import ComicPage from './components/containers/ComicPageContainer';
import store from './store/configureStore';
import ComicAction from './actions/comicActions';
import { Paper } from '@material-ui/core';

class App extends Component {
  componentDidMount() {
    // While this is ugly, and probably confusing, it's a small tweak that I'll get to later
    ComicAction.getMaxCount()(store.dispatch);
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Paper className="paper" style={{ width: "75%" }}>
            <ComicPage />
          </Paper>
        </header>
      </div>
    );
  }
}

export default App;
